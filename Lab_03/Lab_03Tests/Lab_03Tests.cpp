#include "gtest/gtest.h"
#include "../Lab_03/BSTNode.h"
#include "../Lab_03/BST.h"
#include <vector>
#include <algorithm>
#include "../Lab_03/Utils.h"
#include <cstdlib>

BST* removeTree()
{
	BST* tree = new BST();;
	tree->insert(25);
	tree->insert(13);
	tree->insert(3);
	tree->insert(1);
	tree->insert(4);
	tree->insert(17);
	tree->insert(19);
	tree->insert(18);
	tree->insert(23);
	tree->insert(21);
	tree->insert(45);
	tree->insert(30);
	tree->insert(28);
	tree->insert(38);
	tree->insert(33);
	tree->insert(42);
	return tree;

}
//
// BSTNodeTest
//
TEST(BSTNodeTest, parseIntToChar)
{
	BSTNode* node = new BSTNode(4320);
	EXPECT_STREQ("4320", node->getTab());
	delete node;
}

//TEST(BSTNodeTest, expectParentNullOnCreation)
//{
//	BSTNode* node = new BSTNode(432);
//	EXPECT_EQ(nullptr, node->parent);
//	delete node;
//}

TEST(BSTNodeTest, expectRightNullOnCreation)
{
	BSTNode* node = new BSTNode(432);
	EXPECT_EQ(nullptr, node->right);
	delete node;
}

TEST(BSTNodeTest, expectLeftNullOnCreation)
{
	BSTNode* node = new BSTNode(432);
	EXPECT_EQ(nullptr, node->left);
	delete node;
}

//
// BSTTest
//
TEST(BSTTest, rootIsNullOnCreation)
{
	BST tree;
	EXPECT_EQ(tree.root, nullptr);
}

TEST(BSTTestInsert, insertFirstNode)
{
	BST tree;
	tree.insert(23);
	EXPECT_EQ(tree.root->key, 23);
	EXPECT_EQ(tree.root->left, nullptr);
	EXPECT_EQ(tree.root->right, nullptr);
	//EXPECT_EQ(tree.root->parent, nullptr);
	EXPECT_NE(tree.find(23), nullptr);
}

TEST(BSTTestInsert, insertSecondNodeOnLeft)
{
	BST tree;
	tree.insert(23);
	tree.insert(13);
	EXPECT_EQ(tree.root->left->key, 13);
	EXPECT_NE(tree.find(23), nullptr);
	EXPECT_NE(tree.find(13), nullptr);
}

TEST(BSTTestInsert, insertSecondNodeOnRight)
{
	BST tree;
	tree.insert(23);
	tree.insert(3333);
	EXPECT_EQ(tree.root->right->key, 3333);
	EXPECT_NE(tree.find(23), nullptr);
	EXPECT_NE(tree.find(3333), nullptr);
}

TEST(BSTTestFind, findInEmptyTree)
{
	BST tree;
	EXPECT_EQ(tree.find(3), nullptr);
}

TEST(BESTTestFind, notFindInTree)
{
	BST tree;
	tree.insert(23);
	tree.insert(3333);
	EXPECT_EQ(tree.find(4), nullptr);
}

TEST(BSTTestFind, findInTreeWithOneNode)
{
	BST tree;
	tree.insert(3);
	EXPECT_NE(tree.find(3), nullptr);
}

TEST(BSTTestInsertMany, insertManyOneNodeToEmptyTree)
{
	BST tree;
	tree.insertMany(1);
	tree.inOrder();
	EXPECT_NE(tree.root, nullptr);
}

TEST(BSTTestInsertMany, insertManyNodesToEmptyTree)
{
	BST tree;
	tree.insertMany(30);
	EXPECT_EQ(tree.inOrder(), 30);
	EXPECT_EQ(tree.preOrder(), 30);
	EXPECT_EQ(tree.postOrder(), 30);
	EXPECT_NE(tree.root, nullptr);
}

TEST(BSTTestInsertMany, insertManyNodesNotEmpty)
{
	BST tree;
	tree.insert(30);
	EXPECT_EQ(tree.inOrder(), 1);
	tree.insertMany(1000);
	EXPECT_EQ(tree.inOrder(), 1001);
}

TEST(BSTTestInOrder, empty)
{
	BST tree;
	EXPECT_EQ(tree.inOrder(), 0);
}

TEST(BSTTestInOrder, one)
{
	BST tree;
	tree.insert(4);
	EXPECT_EQ(tree.inOrder(), 1);
} 

TEST(BSTTestInOrder, two)
{
	BST tree;
	tree.insert(4);
	tree.insert(5);
	EXPECT_EQ(tree.inOrder(), 2);
	tree.insert(6);
	EXPECT_EQ(tree.inOrder(), 3);
} 
TEST(BSTTestPostOrder, two)
{
	BST tree;
	tree.insert(4);
	tree.insert(5);
	EXPECT_EQ(tree.postOrder(), 2);
	tree.insert(6);
	EXPECT_EQ(tree.postOrder(), 3);
} 

TEST(BSTTestPreOrder, two)
{
	BST tree;
	tree.insert(4);
	tree.insert(5);
	EXPECT_EQ(tree.preOrder(), 2);
	tree.insert(6);
	EXPECT_EQ(tree.preOrder(), 3);
} 

TEST(BSTTestInOrder, many)
{
	BST tree;
	tree.insertMany(20);
	EXPECT_EQ(tree.inOrder(), 20);
}

TEST(BSTTestRemove, remove_in_empty)
{
	BST tree;
	EXPECT_EQ(tree.remove(5), false);
}

TEST(BSTTestRemove, remove_in_only)
{
	BST tree;
	tree.insert(3);
	EXPECT_EQ(tree.remove(3), true);
	EXPECT_EQ(tree.root, nullptr);
	EXPECT_EQ(tree.inOrder(), 0);
}

TEST(BSTTestRemove, remove_leaf)
{
	BST* tree = removeTree();
	EXPECT_EQ(tree->remove(1), true);
	EXPECT_EQ(tree->find(3)->left, nullptr);
	EXPECT_EQ(tree->inOrder(), 15);
	delete(tree);
	
	BST tree2;
	tree2.insert(9);
	tree2.insert(1);
	tree2.insert(5);
	tree2.insert(3);
	tree2.insert(4);
	tree2.insert(7);
	tree2.insert(15);
	tree2.insert(10);
	tree2.insert(12);
	tree2.insert(11);

	EXPECT_EQ(tree2.remove(4), true);
	EXPECT_EQ(tree2.find(3)->right, nullptr);
}

TEST(BSTTestRemove, remove_with_one_child)
{
	BST tree;
	tree.insert(9);
	tree.insert(1);
	tree.insert(5);
	tree.insert(3);
	tree.insert(4);
	tree.insert(7);
	tree.insert(15);
	tree.insert(10);
	tree.insert(12);
	tree.insert(11);

	EXPECT_EQ(tree.remove(4), true);
	EXPECT_EQ(tree.find(3)->right, nullptr);

	EXPECT_EQ(tree.remove(1), true);
	EXPECT_EQ(tree.root->left, tree.find(5));
}

TEST(BSTTestRemove, remove_root_with_two_child)
{
	BST tree;
	tree.insert(9);
	tree.insert(1);
	tree.insert(5);
	tree.insert(3);
	tree.insert(4);
	tree.insert(7);
	tree.insert(15);
	tree.insert(10);
	tree.insert(12);
	tree.insert(11);

	EXPECT_EQ(tree.remove(9), true);
	EXPECT_EQ(tree.root, tree.find(7));
	EXPECT_EQ(tree.find(5)->right, nullptr);
}

TEST(BSTTestRemove, remove_root_with_only_left)
{
	BST tree;
	tree.insert(9);
	tree.insert(1);
	tree.insert(5);
	tree.insert(3);
	tree.insert(4);
	tree.insert(7);
	
	EXPECT_EQ(tree.remove(9), true);
	EXPECT_EQ(tree.root, tree.find(1));
}

TEST(BSTTestRemove, remove_root_with_right_child)
{
	BST tree;
	tree.insert(9);
	tree.insert(15);
	tree.insert(10);
	tree.insert(12);
	tree.insert(11);

	EXPECT_EQ(tree.remove(9), true);
	EXPECT_EQ(tree.root, tree.find(15));
}

TEST(BSTTestRemove, remove_node_with_left_subtree)
{
	BST* tree = removeTree();
	EXPECT_EQ(tree->remove(45), true);
	EXPECT_EQ(tree->root->right, tree->find(30));
	EXPECT_EQ(tree->inOrder(), 15);
	delete tree;
}

TEST(BSTTestRemove, remove_node_with_right_subtree)
{
	BST* tree = removeTree();
	EXPECT_EQ(tree->remove(17), true);
	EXPECT_EQ(tree->find(13)->right, tree->find(19));
	EXPECT_EQ(tree->inOrder(), 15);
	delete(tree);
}

TEST(BSTTestRemove, remove_node_with_two_child_precessor_is_left_child)
{
	BST* tree = removeTree();
	EXPECT_EQ(tree->remove(3), true);
	EXPECT_EQ(tree->find(13)->left->key, tree->find(1)->key);
	EXPECT_EQ(tree->find(1)->right->key, tree->find(4)->key);
	EXPECT_EQ(tree->inOrder(), 15);
	delete(tree);
}

TEST(BSTTestRemove, remove_node_with_two_child_precessor_is_grandchild)
{
	BST* tree = removeTree();
	EXPECT_EQ(tree->remove(13), true);
	EXPECT_EQ(tree->find(25)->left->key, tree->find(4)->key);
	EXPECT_EQ(tree->find(4)->right, tree->find(17));
	EXPECT_EQ(tree->inOrder(), 15);
	delete(tree);
}

TEST(BSTTestRemove, many_remove)
{
	srand(time(NULL));
	int tree_size = 100;
	std::vector <int> list;
	BST tree;
	bool flag = false;
	int i = 0;
	while (i < tree_size) {
		int key = randomInt(-9000, 9000);
		flag = tree.insert(key);
		if (flag) {
			//std::cout << "Dodano: " << key << std::endl;
			list.push_back(key);
			i++;
		}
	}

	std::random_shuffle(list.begin(), list.end());
	for (int i = 0; i < list.size(); i++) {
		tree.insert(list.at(i));
	}
	EXPECT_EQ(tree.inOrder(), tree_size);

	std::random_shuffle(list.begin(), list.end());
	for (int i = 0; i < list.size(); i++) {
		//std::cout << "Usuwanie: " << list.at(i) << std::endl;
		tree.remove(list.at(i));
	}
	EXPECT_EQ(tree.inOrder(), 0);
}
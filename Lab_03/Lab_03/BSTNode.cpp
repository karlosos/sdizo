#include "BSTNode.h"
#include <string>


BSTNode::BSTNode(int key) : key(key)
{
	parseKeyToTab(key);
	//parent = nullptr;
	left = nullptr;
	right = nullptr;
}


BSTNode::~BSTNode()
{
}

char* BSTNode::getTab()
{
	return tab;
}

void BSTNode::parseKeyToTab(int key)
{
	std::string s = std::to_string(key);
	for (int i = 0; i < s.length() && i < 10; i++) {
		tab[i] = s.at(i);
		tab[i + 1] = '\0';
	}
}

//BSTNode* BSTNode::findSuccessor()
//{
//	if (this->right != nullptr)
//		return this->right->findMin();
//	BSTNode* p = this;
//	//BSTNode* parent = this->parent;
//	while (parent != nullptr && p == parent->right) {
//		p = parent;
//		parent = parent->parent;
//	}
//	return parent;
//}

BSTNode* BSTNode::findMin()
{
	BSTNode* p = this;
	while (p->left != nullptr)
		p = p->left;
	return p;
}

BSTNode* BSTNode::findMax()
{
	BSTNode* p = this;
	while (p->right != nullptr)
		p = p->right;
	return p;
}
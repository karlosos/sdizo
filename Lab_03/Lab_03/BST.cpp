#include "BST.h"
#include <iostream>
#include "Utils.h"
#include <ctime>

BST::BST()
{
	root = nullptr;
	count = 0;
}


BST::~BST()
{
	clearTree(root);
}

void BST::clearTree(BSTNode* node)
{
	if (node) {
		if (node->left)
			clearTree(node->left);

		if (node->right)
			clearTree(node->right);

		delete(node);
	}
}

bool BST::insert(int key)
{
	BSTNode* p = root;
	BSTNode* parent = nullptr;
	// looking for a space for a new node
	while (p != nullptr)
	{
		// key already exists
		if (p->key == key) 
			return false;
		parent = p;
		if (p->key > key)
			p = p->left;
		else
			p = p->right;
	}
	if (root == nullptr)
		root = new BSTNode(key);
	else if (parent->key > key) {
		parent->left = new BSTNode(key);
	}
	else {
		parent->right = new BSTNode(key);
	}
	return true;
}

void BST::insertMany(int quantity)
{
	srand(time(NULL));
	for (int i = 0; i < quantity; i++) {
		bool flag = false;
		while (!flag) {
			flag = insert(randomInt(-10000, 10000));
		}
	}
}

bool BST::remove(int key)
{
	BSTNode* parent = nullptr;
	BSTNode* p = root;
	// find node
	// todo dry
	while (p != nullptr && key != p->key) {
		parent = p;
		if (p->key < key)
			p = p->right;
		else
			p = p->left;
	}
	if (p == nullptr)
		return false;
	// removing leaf 
	if (p->right == nullptr && p->left == nullptr) {
		if (p == root) {
			root = nullptr;
			delete(p);
			return true;
		}
		if (parent->right == p) {
			parent->right = nullptr;
			delete(p);
			return true;
		}
		else {
			parent->left = nullptr;
			delete(p);
			return true;
		}
	}

	// has only left subtree
	if (p->right == nullptr) {
		if (parent) {
			if (parent->right == p) {
				parent->right = p->left;
			}
			else {
				parent->left = p->left;
			}
		}
		else {
			root = p->left;
		}
		delete(p);
		return true;
	}

	// has only right subtree
	if (p->left == nullptr) {
		if (parent) {
			if (parent->right == p) {
				parent->right = p->right;
			}
			else {
				parent->left = p->right;
			}
		}
		else {
			root = p->right;
		}
		delete(p);
		return true;
	}
	
	// has two subtrees
	BSTNode* preparent = p;		// parent of precessor
	BSTNode* child = p->left;	// precessor
	// finding precessor
	while (child->right != nullptr) {
		preparent = child;
		child = child->right;
	}

	// precessor is left child of node
	if (child == p->left) {
		if (parent) {
			if (parent->right == p) {
				parent->right = child;
				child->right = p->right;
			}
			else {
				parent->left = child;
				child->right = p->right;
			}
		}
		else {
			root = child;
			child->right = p->right;
		}
		delete(p);
		return true;
	}

	// precessor is not left child but grandchild ...
	BSTNode* grandchild = child->left;
	if (preparent->right == child)
		preparent->right = grandchild;
	else
		preparent->left = grandchild;

	child->left = p->left;
	if (parent) {
		if (parent->right == p) {
			parent->right = child;
			child->right = p->right;
		}
		else {
			parent->left = child;
			child->right = p->right;
		}
	}
	else {
		root = child;
		child->right = p->right;
	}
	delete(p);
	return true;
}

BSTNode* BST::find(int key)
{
	BSTNode* p = root;
	while ((p != nullptr) && (key != p->key)) {
		if (p->key < key)
			p = p->right;
		else
			p = p->left;
	}
	if (p == nullptr)
		std::cout << "Nie znaleziono klucza: " << key << std::endl;
	return p;
}

int BST::inOrder()
{
	std::cout << "==========" << std::endl;
	std::cout << "In order" << std::endl;
	count = 0;
	inOrder(root);
	std::cout << "Size: " << count << std::endl;
	std::cout << "==========" << std::endl;
	return count;
}

int BST::inOrder(BSTNode *node)
{
	if (node != nullptr) {
		inOrder(node->left);
		count++;
		std::cout << node->key << std::endl;
		inOrder(node->right);
	}
	return 0;
}

int BST::preOrder()
{
	std::cout << "==========" << std::endl;
	std::cout << "Pre order" << std::endl;
	count = 0;
	preOrder(root);
	std::cout << "Size: " << count << std::endl;
	std::cout << "==========" << std::endl;
	return count;
}

int BST::preOrder(BSTNode *node)
{
	if (node != nullptr) {
		count++;
		std::cout << node->key << std::endl;
		preOrder(node->left);
		preOrder(node->right);
	}
	return 0;
}

int BST::postOrder()
{
	std::cout << "==========" << std::endl;
	std::cout << "Post order" << std::endl;
	count = 0;
	postOrder(root);
	std::cout << "Size: " << count << std::endl;
	std::cout << "==========" << std::endl;
	return count;
}

int BST::postOrder(BSTNode *node)
{
	if (node != nullptr) {
		postOrder(node->left);
		postOrder(node->right);
		count++;
		std::cout << node->key << std::endl;
	}
	return 0;
}

#pragma once
#include <stack>
class BSTNode
{
public:
	BSTNode(int key);
	~BSTNode();
	char* getTab();
	//BSTNode* parent;
	BSTNode* left;
	BSTNode* right;
	const int key;
	//BSTNode* findSuccessor();
	BSTNode* findMin();
	BSTNode* findMax();

private:
	char tab[10];
	void parseKeyToTab(int key);
};


#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <stdio.h>

int randomInt() {
	return rand() % 1000 + 1;
}

char randomChar() {
	return char(rand() % 25 + 65);
}

struct Struktura
{
	int calkowita;
	char znak;
	float numer;
};

Struktura** losowanie(int N) {
	Struktura** struktury = new Struktura*[N];
	for (int i = 0; i < N; i++) {
		struktury[i] = new Struktura();
		struktury[i]->calkowita = randomInt();
		struktury[i]->znak = randomChar();
		struktury[i]->numer = 100 + i + 1;
	}
	return struktury;
}

void kasowanie(Struktura** struktury, int N) {
	for (int i = 0; i < N; i++) {
		delete struktury[i];
	}

	delete[] struktury;
}

void sortowanie(Struktura** A, int N) {
	// od pierwszego elementu do przedostatniego (bo sprawdzamy nastepny)
	for (int i = 0; i < N - 1; i++) {
		bool flag = false;
		// od pierwszego elementu do przedostatniego - numer iteracji
		for (int j = 0; j < N - i - 1; j++) {
			if (A[j]->calkowita > A[j + 1]->calkowita)
			{
				Struktura* tmp = A[j + 1];
				A[j + 1] = A[j];
				A[j] = tmp;
				flag = true;
			}
		}
		if (flag == false)
			return;
	}
}

int zliczanie(Struktura** A, int N, char znak) {
	int suma = 0;
	for (int i = 0; i < N; i++) {
		if (A[i]->znak == znak)
			suma++;
	}
	return suma;
}

void wyswietl(Struktura** struktury, int liczba_struktur) {
	for (int i = 0; i < liczba_struktur; i++) {
		std::cout << "Struktura " << i << ": " << struktury[i]->calkowita << " " << struktury[i]->znak << " " << struktury[i]->numer << std::endl;
	}
}
int main()
{
	srand(time(NULL));
	int N;
	char X;
	int suma;

	FILE* fp = fopen("inlab01.txt", "r");
	if (fp == NULL)
		return -1;
	fscanf(fp, "%d %c", &N, &X);
	fclose(fp);

	clock_t begin, end;
	double time_spent;
	begin = clock();
	Struktura** struktury = losowanie(N);

	sortowanie(struktury, N);
	suma = zliczanie(struktury, N, X);
	wyswietl(struktury, 20);
	kasowanie(struktury, N);
	end = clock();
	time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
	std::cout << "Ilosc znaku " << X << " " << suma << std::endl;
	std::cout << "Czas " << time_spent << std::endl;
	std::system("pause");

	return 0;
}
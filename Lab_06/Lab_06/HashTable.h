//
// Created by Karol Dzialowski on 22.12.2017.
//

#ifndef LAB_06_HASHTABLE_H
#define LAB_06_HASHTABLE_H

#include "LinearAddressingStrategy.h"
#include "SimpleAddressingStrategy.h"
#include "../Lab_06Tests/googletest/googletest/include/gtest/gtest_prod.h"
#include "Utils.h"
#include <iostream>

class HashTable {
public:
    HashTable();
    int get(int key);
    bool put(int key);
    bool remove(int key);
    void setAddressingStrategy(int type);
    void putMany(int quantity);
    void print(int start, int stop);
    int checkLength();

private:
    int table[997];
    AddressingStrategy *addressing_strategy;

    FRIEND_TEST(PutMany, test_put_many);
};


#endif //LAB_06_HASHTABLE_H

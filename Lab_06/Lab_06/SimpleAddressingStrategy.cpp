//
// Created by karol on 1/5/18.
//

#include "SimpleAddressingStrategy.h"

int SimpleAddressingStrategy::alternativeFunction(int key, int i) {
    int h = (3*key)%19 + 1;
    h = h*i;
    return h;
}
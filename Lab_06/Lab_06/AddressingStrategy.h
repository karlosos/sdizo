//
// Created by karol on 1/5/18.
//

#ifndef LAB_06_ADDRESSINGSTRATEGY_H
#define LAB_06_ADDRESSINGSTRATEGY_H
#include <math.h>

class AddressingStrategy {
public:
    int hashFunction(int key);
    virtual int alternativeFunction(int key, int i) = 0;
};


#endif //LAB_06_ADDRESSINGSTRATEGY_H

//
// Created by karol on 1/5/18.
//

#include "AddressingStrategy.h"

int AddressingStrategy::hashFunction(int key) {
    int h = ((key % 1000) + int(pow(2, key % 10)) + 1 ) % 997;
    return h;
}
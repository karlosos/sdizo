//
// Created by Karol Dzialowski on 22.12.2017.
//

#include "HashTable.h"


HashTable::HashTable() {
    for (int i=0; i<997; i++) {
        table[i] = 0;
    }

    addressing_strategy = nullptr;
}

int HashTable::get(int key) {
    int h = addressing_strategy->hashFunction(key);
    if(table[h] == 0) {
        std::cout << "Brak klucza " << key << std::endl;
        return 0;
    }
    else if(table[h] > 0) {
        if (table[h] == key)
            return table[h];
    }

    int i = 1;
    h = addressing_strategy->alternativeFunction(key, i);
    while (h < 997) {
        if (table[h] == 0) {
            std::cout << "Brak klucza " << key << std::endl;
            return 0;
        }
        else if (table[h] > 0) {
            if (table[h] == key)
                return table[h];
        }
        i++;
        h = addressing_strategy->alternativeFunction(key, i);
    }

    return 0;
}

bool HashTable::put(int key) {
    int h = addressing_strategy->hashFunction(key);
    if(table[h] <= 0) {
        table[h] = key;
        return true;
    }

    int i = 1;
    h = addressing_strategy->alternativeFunction(key, i);
    while (h < 997) {
        if (table[h] <= 0) {
            table[h] = key;
            return true;
        }
        else if (table[h] > 0) {
            if (table[h] == key) {
                return false;
            }
        }
        i++;
        h = addressing_strategy->alternativeFunction(key, i);
    }

    return false;
}

bool HashTable::remove(int key) {
    int h = addressing_strategy->hashFunction(key);
    if(table[h] == 0) {
        std::cout << "Brak klucza " << key << std::endl;
        return false;
    }
    else if(table[h] > 0) {
        if (table[h] == key) {
            table[h] = -1;
            std::cout << "Usunieto klucz " << key << " z indeksu " << h << std::endl;
            return true;
        }
    }

    int i = 1;
    h = addressing_strategy->alternativeFunction(key, i);
    while (h < 997) {
        if (table[h] == 0) {
            std::cout << "Brak klucza " << key << std::endl;
            return 0;
        }
        else if (table[h] > 0) {
            if (table[h] == key) {
                table[h] = -1;
                std::cout << "Usunieto klucz " << key << " z indeksu " << h << std::endl;
                return true;
            }
        }
        i++;
        h = addressing_strategy->alternativeFunction(key, i);
    }

    return false;
}

void HashTable::setAddressingStrategy(int type) {
    delete addressing_strategy;
    if (type == 0) {
        addressing_strategy = new LinearAddressingStrategy();
    } else {
        addressing_strategy = new SimpleAddressingStrategy();
    }
}

void HashTable::putMany(int quantity) {
    int key;
    while (quantity>0) {
        key = randomInt(20000, 40000);
        if(put(key))
            quantity--;
    }
}

void HashTable::print(int start, int stop) {
    for (int i=start; i<=stop; i++) {
        std::cout << "index: " << i << " key: " << table[i] << std::endl;
    }
}

int HashTable::checkLength() {
    int q = 0;
    for (int i=0; i<997; i++) {
        if (table[i] > 0)
            q++;
    }
    return q;
}
//
// Created by karol on 1/5/18.
//

#ifndef LAB_06_SIMPLEADDRESSINGSTRATEGY_H
#define LAB_06_SIMPLEADDRESSINGSTRATEGY_H
#include "AddressingStrategy.h"

class SimpleAddressingStrategy : public AddressingStrategy {
    int alternativeFunction(int key, int i);
};


#endif //LAB_06_SIMPLEADDRESSINGSTRATEGY_H

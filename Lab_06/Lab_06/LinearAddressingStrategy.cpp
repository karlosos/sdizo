//
// Created by karol on 1/5/18.
//

#include "LinearAddressingStrategy.h"

int LinearAddressingStrategy::alternativeFunction(int key, int i)
{
    int h = AddressingStrategy::hashFunction(key) + i;
    return h;
}
//
// Created by karol on 1/5/18.
//

#ifndef LAB_06_LINEARADDRESSINGSTRATEGY_H
#define LAB_06_LINEARADDRESSINGSTRATEGY_H

#include "AddressingStrategy.h"
class LinearAddressingStrategy : public AddressingStrategy {
    int alternativeFunction(int key, int i);
};


#endif //LAB_06_LINEARADDRESSINGSTRATEGY_H

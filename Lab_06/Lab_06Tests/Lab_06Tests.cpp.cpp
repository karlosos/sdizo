//
// Created by Karol Dzialowski on 22.12.2017.
//
#include "gtest/gtest.h"
#include "../Lab_06/HashTable.h"

TEST(Initialization, init_with_zeroes)
{
    HashTable ht;
    ht.setAddressingStrategy(1);
    EXPECT_EQ(ht.get(2), 0);
}

TEST(Put, test_from_inlab)
{
    HashTable ht;
    ht.setAddressingStrategy(1);
    ht.put(500);
    EXPECT_EQ(ht.get(500), 500);
    ht.put(25013);
    EXPECT_EQ(ht.get(25013), 25013);
    ht.put(29511);
    EXPECT_EQ(ht.get(29511), 29511);
    ht.put(37013);
    EXPECT_EQ(ht.get(37013), 37013);
}

TEST(Collision, test_collision)
{
    HashTable ht;
    ht.setAddressingStrategy(1);
    ht.put(529);
    EXPECT_EQ(ht.get(529), 529);
    ht.put(1529);
    EXPECT_EQ(ht.get(1529), 1529);
    ht.put(2529);
    EXPECT_EQ(ht.get(2529), 2529);
}

TEST(Collision, test_collision_liniowe)
{
    HashTable ht;
    ht.setAddressingStrategy(0);
    ht.put(529);
    EXPECT_EQ(ht.get(529), 529);
    ht.put(1529);
    EXPECT_EQ(ht.get(1529), 1529);
    ht.put(2529);
    EXPECT_EQ(ht.get(2529), 2529);
}

TEST(Remove, test_remove)
{
    HashTable ht;
    ht.setAddressingStrategy(1);
    ht.put(529);
    ht.put(1529);
    EXPECT_EQ(ht.remove(529), true);
    ht.put(2529);
    ht.remove(2529);
    ht.put(3529);
}

TEST(Remove, test_remove_not_existing)
{
    HashTable ht;
    ht.setAddressingStrategy(1);
    ht.put(529);
    EXPECT_EQ(ht.remove(1529), false);
}

TEST(PutMany, test_put_many)
{
    HashTable ht;
    ht.setAddressingStrategy(1);
    ht.putMany(500);
    int q = 0;
    for (int i=0; i<997; i++) {
        if (ht.table[i] > 0)
            q++;
    }
    EXPECT_EQ(q, 500);
}
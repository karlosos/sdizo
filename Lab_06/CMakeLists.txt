cmake_minimum_required(VERSION 3.8)
project(Lab_06)

set(CMAKE_CXX_STANDARD 11)
set(SOURCE_FILES main.cpp)
add_executable(runLab06 ${SOURCE_FILES})
add_subdirectory(Lab_06Tests)
add_subdirectory(Lab_06)
target_link_libraries(runLab06 Lab_06)
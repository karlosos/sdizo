#include <iostream>
#include <ctime>
#include "Lab_06/HashTable.h"

int main() {
    int keys[5];
    FILE* fp = fopen("inlab06.txt", "r");
    if (fp == NULL)
        return -1;
    fscanf(fp, "%d %d %d %d %d", &keys[0], &keys[1], &keys[2], &keys[3], &keys[4]);
    fclose(fp);

    // mieszanie liniowe
    clock_t begin, end;
    double time_spent;
    begin = clock();

    HashTable ht_liniowe;
    ht_liniowe.setAddressingStrategy(0);
    ht_liniowe.remove(keys[1]);
    ht_liniowe.put(keys[1]);
    ht_liniowe.print(0, 100);
    ht_liniowe.putMany(keys[0]);
    std::cout << "------------" << ht_liniowe.checkLength() << std::endl;
    ht_liniowe.print(0, 100);
    ht_liniowe.put(keys[2]);
    ht_liniowe.put(keys[3]);
    ht_liniowe.put(keys[4]);
    ht_liniowe.print(0, 100);
    ht_liniowe.print(500, 600);
    ht_liniowe.remove(keys[3]);
    ht_liniowe.remove(keys[4]);
    ht_liniowe.print(0, 100);
    ht_liniowe.print(500, 600);

    end = clock();
    time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    std::cout << "Time spent liniowe: " << time_spent << std::endl;

    // mieszanie podwojne
    begin = clock();

    HashTable ht_podwojne;
    ht_podwojne.setAddressingStrategy(1);
    ht_podwojne.remove(keys[1]);
    ht_podwojne.put(keys[1]);
    ht_podwojne.print(0, 100);
    ht_podwojne.putMany(keys[0]);
    ht_podwojne.print(0, 100);
    ht_podwojne.put(keys[2]);
    ht_podwojne.put(keys[3]);
    ht_podwojne.put(keys[4]);
    ht_podwojne.print(0, 100);
    ht_podwojne.print(500, 600);
    ht_podwojne.remove(keys[3]);
    ht_podwojne.remove(keys[4]);
    ht_podwojne.print(0, 100);
    ht_podwojne.print(500, 600);

    end = clock();
    time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    std::cout << "Time spent podwojne: " << time_spent << std::endl;
    return 0;
}
// SDIZO I1 212A LAB01
// Karol Dzialowski
// dk39259@zut.edu.pl

#define _CRT_SECURE_NO_DEPRECATE

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <cstdlib>
#include <ctime>
#include <set>


struct Struktura {
	int i;
	char c;
	float f;
};

int randomInt(int startowa_liczba=-1000, int ostatnia_liczba=9000) {
	return rand() % (ostatnia_liczba - startowa_liczba) + startowa_liczba;
}

char randomChar(char startowy_znak='B', char ostatni_znak='X') {
	return rand() % (ostatni_znak - startowy_znak) + startowy_znak;
}

Struktura** losowanie(int N) {
	struct Struktura** tab = (struct Struktura**) malloc(N * sizeof(struct Struktura*));
	std::set<int> wykluczone;
	for (int i = 0; i < N; i++) {
		tab[i] = (struct Struktura*) malloc(sizeof(struct Struktura));
		
		// dodawanie unikalnych i
		int struct_i;
		do {
			struct_i = randomInt();
		} while (wykluczone.find(struct_i) != wykluczone.end());

		wykluczone.insert(struct_i);
		tab[i]->i = struct_i;

		tab[i]->c = randomChar();
		tab[i]->f = 1001.f + i;
	}

	return tab;
}

void kasowanie(Struktura** tab, int N) {
	for (int i = 0; i < N; i++)
		free(tab[i]);
	free(tab);
}

void sortowanie(Struktura** tab, int N) {
	for (int i = 0; i < N; i++) {
		bool flag = false;
		for (int j = 0; j < N - i - 1; j++) {
			if (tab[j]->i > tab[j + 1]->i) {
				Struktura* tmp = tab[j];
				tab[j] = tab[j + 1];
				tab[j + 1] = tmp;
				flag = true;
			}
		}
		if (flag == false)
			return;
	}
}

void wyswietl(Struktura** tab, int N) {
	for (int i = 0; i < N; i++) {
		std::cout << tab[i]->i << " " << tab[i]->c << " " << tab[i]->f << std::endl;
	}
}

int zliczanie(Struktura** tab, int N, char c) {
	int licznik = 0;
	for (int i = 0; i < N; i++) {
		if (tab[i]->c == c)
			licznik++;
	}
	return licznik;
}

int main() {
	srand(time(NULL));
	int N;
	char X;
	FILE* fp = fopen("inlab01.txt", "r");
	if (fp == NULL)
		return -1;
	fscanf(fp, "%d %c", &N, &X);
	fclose(fp);
	clock_t begin, end;
	double time_spent;
	begin = clock();

	Struktura** tab = losowanie(N);
	sortowanie(tab, N);
	wyswietl(tab, 20);
	std::cout << "Zliczanie znaku " << X << " " << zliczanie(tab, N, X) << std::endl;
	kasowanie(tab, N);

	end = clock();
	time_spent = (double)(end - begin);
	std::cout << "Time spent: " << time_spent << std::endl;
	std::system("pause");
	return 0;
}
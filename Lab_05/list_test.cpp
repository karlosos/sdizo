#include "Utils.h"
#include "BidirectionalCircularList.h"
#include <vector>
#include <ctime>


int main(int argc, char *argv[]) {
    BidirectionalCircularList lista;

    int ilosc_kluczy = atoi(argv[1]);
    bool czy_losowy = atoi(argv[2]);
    std::vector<int> klucze;
    clock_t begin, end;
    double time_spent;

    // zawsze to samo losowanie
    srand(1337);


    //
    // Wstawianie
    //

    // losowanie kluczy
    klucze = generujKlucze(ilosc_kluczy, czy_losowy);
    begin = clock();

    // wstawianie danych
    for (int i=0; i<ilosc_kluczy; i++) {
        lista.append(klucze.at(i));
    }

    end = clock();
    time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    int ilosc_kluczy_wstawionych = lista.getLength();
    std::cout << "Wstawianie " << ilosc_kluczy<< " elementow lista: : " << time_spent << std::endl;

    //
    // Wyszukiwanie
    //

    // losowanie kluczy
    klucze.empty();
    klucze = generujKlucze(ilosc_kluczy, true);
    begin = clock();
    // wstawianie danych
    for (int i=0; i<ilosc_kluczy; i++) {
        lista.find(klucze.at(i));
    }
    end = clock();
    time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    std::cout << "Wyszukiwanie " << ilosc_kluczy << " elementow lista: : " << time_spent << std::endl;

    //
    // Usuwanie
    //
    // losowanie kluczy
    klucze.empty();
    klucze = generujKlucze(ilosc_kluczy, true);
    begin = clock();
    // wstawianie danych
    for (int i=0; i<ilosc_kluczy; i++) {
        lista.remove(klucze.at(i));
    }
    end = clock();
    time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    std::cout << "Usuwanie " << ilosc_kluczy << " elementow lista: : " << time_spent << std::endl;

    return 0;
}
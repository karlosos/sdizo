#pragma once
#include "BSTNode.h"
class BST
{
public:
	BST();
	~BST();
	BSTNode* root;
	bool insert(int key);
	void insertMany(int key);
	BSTNode* find(int key);
	bool remove(int key);	

	int inOrder();
	int inOrder(BSTNode* node);
	int preOrder();
	int preOrder(BSTNode* node);
	int postOrder();
	int postOrder(BSTNode* node);
private:
	int count;
	void clearTree(BSTNode* node);
};


#pragma once
#include <stack>
class BSTNode
{
public:
	BSTNode(int key);
	~BSTNode();
	BSTNode* left;
	BSTNode* right;
	const int key;
};


#include "AVL.h"
#include "Utils.h"

AVL::AVL()
{
	root = nullptr;
}

AVL::~AVL()
{
	clearTree(root);
	root = nullptr;
}

void AVL::clearTree(AVLNode* node)
{
	if (node) {
		if (node->left)
			clearTree(node->left);

		if (node->right)
			clearTree(node->right);

		delete(node);
	}
}

bool AVL::insert(int key)
{
	// faza 1
	// wstawiamy nowy wezel jako lisc i pamietamy do niego droge
	std::vector <AVLNode*> road_to_root = insertAsLeaf(key);
	if (road_to_root.back() == nullptr)
		return false;

	//std::cout << "Dodaje: " << key << std::endl;

	bool flag = true;
	for (int i=road_to_root.size()-2; i>=0; i--) {
		AVLNode *parent = road_to_root.at(i);
		AVLNode *child = road_to_root.at(i + 1);
		child->height = 1 + max(getHeight(child->left), getHeight(child->right));
		parent->height = 1 + max(getHeight(parent->left), getHeight(parent->right));

		int balance = getBalanceFactor(parent);

		if (balance == 0)
			return true;

		if (balance == 2 || balance == -2) {
			AVLNode *grandchild = nullptr;
			if (i + 2 < road_to_root.size())
				grandchild = road_to_root.at(i + 2);
			AVLNode *grandparent = nullptr;
			if (i - 1 >= 0)
				grandparent = road_to_root.at(i - 1);
			// zmiana +1 na +2
			// jezeli prawy potomek wezla w ktorym nastapila zmiana wspolczynnika wywazenia +1 -> +2 mial
			// przed wstawieniem nowego wezla wspolczynnik wywazenia 0, zas po wstawieniu nowego wezla ulegla
			// zwiekszeniu wysokosc prawego poddrzewa tego potomka i korzen tego poddrzewa przed wstawieniem
			// nowego wezla mial wspolczynnik wywazenia 0 lub nie istnial to do przywrocenia lokalnego i
			// globalnego wywazenia wystarczy pojedyncza rotacja tego prawego potomka w lewo
			if (parent->right == child) {
				// sytuacja jednorodna
				if (child->right == grandchild) {
					rotateLeft(grandparent, parent, child);
				}
					// sytuacja niejednorodna
				else {
					rotateRight(parent, child, grandchild);
					rotateLeft(grandparent, parent, grandchild);
				}
			} else {
				if (child->left == grandchild) {
					rotateRight(grandparent, parent, child);
				} else {
					rotateLeft(parent, child, grandchild);
					rotateRight(grandparent, parent, grandchild);
				}
			}
			return true;
		}

	}
	return true;
}

void AVL::insertMany(int quantity)
{
	srand(time(NULL));
	for (int i = 0; i < quantity; i++) {
		bool flag = false;
		while (!flag) {
			flag = insert(randomInt(-10000, 10000));
		}
	}
}

std::vector<AVLNode*> AVL::insertAsLeaf(int key)
{
	// stos na ktorym pamietamy cala sciezke od korzenia do nowego wezla, ze wspolczynnikami wywazenia
	std::vector <AVLNode*> road_to_root;
	AVLNode* p = root;
	AVLNode* parent = nullptr;
	// looking for a space for a new node
	while (p != nullptr)
	{
		// key already exists
		if (p->key == key) {
			road_to_root.clear();
			road_to_root.push_back(nullptr);
			return road_to_root;
		}
		parent = p;
		road_to_root.push_back(parent);
		if (p->key > key)
			p = p->left;
		else
			p = p->right;
	}
	if (root == nullptr) {
		root = new AVLNode(key);
		road_to_root.push_back(root);
	}
	else if (parent->key > key) {
		parent->left = new AVLNode(key);
		road_to_root.push_back(parent->left);
	}
	else {
		parent->right = new AVLNode(key);
		road_to_root.push_back(parent->right);
	}
	return road_to_root;
}

void AVL::rotateRight(AVLNode* grandfather, AVLNode* parent, AVLNode* child)
{
	if (parent == nullptr)
		return;

	if (grandfather != nullptr)
	{
		if (grandfather->right == parent)
			grandfather->right = child;
		else
			grandfather->left = child;
	}
	else {
		root = child;
	}
	AVLNode* tmp = child->right;
	child->right = parent;
	parent->left = tmp;

	parent->height = std::max(getHeight(parent->left), getHeight(parent->right)) + 1;
	child->height = std::max(getHeight(child->left), getHeight(child->right)) + 1;
	if (grandfather)
		grandfather->height = std::max(getHeight(grandfather->left), getHeight(grandfather->right)) + 1;
}

void AVL::rotateLeft(AVLNode* grandfather, AVLNode* parent, AVLNode* child)
{
	if (grandfather != nullptr)
	{
		if (grandfather->right == parent)
			grandfather->right = child;
		else
			grandfather->left = child;
	}
	else {
		root = child;
	}
	AVLNode* tmp = child->left;
	child->left = parent;
	parent->right = tmp;

	parent->height = std::max(getHeight(parent->left), getHeight(parent->right)) + 1;
	child->height = std::max(getHeight(child->left), getHeight(child->right)) + 1;
	if (grandfather)
		grandfather->height = std::max(getHeight(grandfather->left), getHeight(grandfather->right)) + 1;
}

bool AVL::remove(int key)
{
	//std::cout << "Usuwanie: " << key << std::endl;
	AVLNode* parent = nullptr;
	AVLNode* p = root;
	// find node
	// todo dry
	//std::vector<AVLNode*> road_to_root;
	std::vector<AVLNode*> road_to_root;
	while (p != nullptr && key != p->key) {
		parent = p;
		road_to_root.push_back(p);
		if (p->key < key)
			p = p->right;
		else
			p = p->left;
	}
	if (p == nullptr) {
		//std::cout << "Klucz :" << key << " nie istnieje" << std::endl;
		return false;
	}
	// removing leaf
	if (p->right == nullptr && p->left == nullptr) {
		if (p == root) {
			root = nullptr;
			delete(p);
			return true;
		}
		if (parent->right == p) {
			parent->right = nullptr;
			delete(p);
			balanceAfterRemove(road_to_root);
			return true;
		}
		else {
			parent->left = nullptr;
			delete(p);
			balanceAfterRemove(road_to_root);
			return true;
		}
	}

	// has only left subtree
	if (p->right == nullptr) {
		if (parent) {
			if (parent->right == p) {
				parent->right = p->left;
			}
			else {
				parent->left = p->left;
			}
		}
		else {
			root = p->left;
		}
		delete(p);
		balanceAfterRemove(road_to_root);
		return true;
	}

	// has only right subtree
	if (p->left == nullptr) {
		if (parent) {
			if (parent->right == p) {
				parent->right = p->right;
			}
			else {
				parent->left = p->right;
			}
		}
		else {
			root = p->right;
		}
		delete(p);
		balanceAfterRemove(road_to_root);
		return true;
	}

//	std::queue <AVLNode*> road_from_deleted_to_preccesor;
	std::queue <AVLNode*> road_from_deleted_to_preccesor;
	// has two subtrees
	AVLNode* preparent = p;		// parent of precessor
	AVLNode* child = p->left;	// precessor
	// finding precessor
	while (child->right != nullptr) {
		preparent = child;
		road_from_deleted_to_preccesor.push(preparent);
		child = child->right;
	}

	// precessor is left child of node
	if (child == p->left) {
		if (parent) {
			if (parent->right == p) {
				parent->right = child;
				child->right = p->right;
			}
			else {
				parent->left = child;
				child->right = p->right;
			}
		}
		else {
			root = child;
			child->right = p->right;
		}
		delete(p);
		road_to_root.push_back(child);
		balanceAfterRemove(road_to_root);
		return true;
	}

	// precessor is not left child but grandchild ...
	AVLNode* grandchild = child->left;
	if (preparent->right == child)
		preparent->right = grandchild;
	else
		preparent->left = grandchild;

	child->left = p->left;
	if (parent) {
		if (parent->right == p) {
			parent->right = child;
			child->right = p->right;
		}
		else {
			parent->left = child;
			child->right = p->right;
		}
	}
	else {
		root = child;
		child->right = p->right;
	}
	delete(p);
	road_to_root.push_back(child);
	while (road_from_deleted_to_preccesor.size()>0) {
		AVLNode* to_push = road_from_deleted_to_preccesor.front();
		road_to_root.push_back(to_push);
		road_from_deleted_to_preccesor.pop();
	}
	balanceAfterRemove(road_to_root);
	return true;
}

void AVL::balanceAfterRemove(std::vector<AVLNode*> road_to_root) {
	for (int i = road_to_root.size() - 2; i >= 0; i--) {
		AVLNode* parent = road_to_root.at(i);
		AVLNode* child = road_to_root.at(i + 1);

		child->height = 1 + max(getHeight(child->left), getHeight(child->right));
		parent->height = 1 + max(getHeight(parent->left), getHeight(parent->right));

		int balance = getBalanceFactor(parent);

		if (balance == 1 || balance == -1)
			return;

		bool flag = true;

		if (flag) {
			AVLNode *grandparent = nullptr;
			AVLNode *grandchild = nullptr;
			if (i - 1 >= 0)
				grandparent = road_to_root.at(i - 1);
			if (balance == 2) {
				if (getBalanceFactor(parent->right) == 1)
					rotateLeft(grandparent, parent, parent->right);
				else if (getBalanceFactor(parent->right) == -1) {
					grandchild = parent->right->left;
					rotateRight(parent, parent->right, grandchild);
					rotateLeft(grandparent, parent, grandchild);
				} else if (getBalanceFactor(parent->right) == 0) {
					rotateLeft(grandparent, parent, parent->right);
					flag = false;
				}
			} else if (balance == -2) {
				if (getBalanceFactor(parent->left) == -1)
					rotateRight(grandparent, parent, parent->left);
				else if (getBalanceFactor(parent->left) == 1) {
					grandchild = parent->left->right;
					rotateLeft(parent, parent->left, grandchild);
					rotateRight(grandparent, parent, grandchild);
				} else if (getBalanceFactor(parent->left) == 0) {
					rotateRight(grandparent, parent, parent->left);
					flag = false;
				}
			}
		}
	}
}
AVLNode* AVL::find(int key)
{
	AVLNode* p = root;
	while ((p != nullptr) && (key != p->key)) {
		if (p->key < key)
			p = p->right;
		else
			p = p->left;
	}
	if (p == nullptr) {
		//std::cout << "Nie znaleziono klucza: " << key << std::endl;
	}
	return p;
}

int AVL::inOrder()
{
	std::cout << "==========" << std::endl;
	std::cout << "In order" << std::endl;
	count = 0;
	inOrder(root);
	std::cout << "Size: " << count << std::endl;
	std::cout << "==========" << std::endl;
	return count;
}

int AVL::inOrder(AVLNode *node)
{
	if (node != nullptr) {
		inOrder(node->left);
		count++;
		std::cout << node->key << " balace:" << getBalanceFactor(node) << " tablica:" << node->getTab() << std::endl;
		inOrder(node->right);
	}
	return 0;
}

int AVL::preOrder()
{
	std::cout << "==========" << std::endl;
	std::cout << "Pre order" << std::endl;
	count = 0;
	preOrder(root);
	std::cout << "Size: " << count << std::endl;
	std::cout << "==========" << std::endl;
	return count;
}

int AVL::preOrder(AVLNode *node)
{
	if (node != nullptr) {
		count++;
		std::cout << node->key << " balace:" << getBalanceFactor(node) << " tablica:" << node->getTab() << std::endl;
		preOrder(node->left);
		preOrder(node->right);
	}
	return 0;
}

int AVL::postOrder()
{
	std::cout << "==========" << std::endl;
	std::cout << "Post order" << std::endl;
	count = 0;
	postOrder(root);
	std::cout << "Size: " << count << std::endl;
	std::cout << "==========" << std::endl;
	return count;
}

int AVL::postOrder(AVLNode *node)
{
	if (node != nullptr) {
		postOrder(node->left);
		postOrder(node->right);
		count++;
		std::cout << node->key << " balace:" << getBalanceFactor(node) << std::endl;
	}
	return 0;
}


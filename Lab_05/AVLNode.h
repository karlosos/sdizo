#pragma once
class AVLNode
{
public:
    AVLNode(int key);
    ~AVLNode();

    const int key;
    AVLNode* left;
    AVLNode* right;
    int height;

    char* getTab();
private:
    char tab[10];
    void parseKeyToTab(int key);

};

int getHeight(AVLNode *N);
int getBalanceFactor(AVLNode *N);
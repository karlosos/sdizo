#include <string>
#include "AVLNode.h"



AVLNode::AVLNode(int key) : key(key)
{
    //parseKeyToTab(key);
    left = nullptr;
    right = nullptr;
    height = 1;
}


AVLNode::~AVLNode()
{
}

char* AVLNode::getTab()
{
    return tab;
}

void AVLNode::parseKeyToTab(int key)
{
    std::string s = std::to_string(key);
    for (int i = 0; i < s.length() && i < 10; i++) {
        tab[i] = s.at(i);
        tab[i + 1] = '\0';
    }
}

int getHeight(AVLNode *node)
{
    if (node == nullptr)
        return 0;
    return node->height;
}

int getBalanceFactor(AVLNode *node) {
    if (node == nullptr)
        return 0;
    return getHeight(node->right) - getHeight(node->left);
}


#include "Utils.h"
#include "BST.h"
#include <vector>
#include <ctime>
#include <stdlib.h>
#include <iostream>

int main(int argc, char *argv[]) {
    BST drzewo;

    int ilosc_kluczy = atoi(argv[1]);
    bool czy_losowy = atoi(argv[2]);
    std::vector<int> klucze;
    clock_t begin, end;
    double time_spent;

    // zawsze to samo losowanie
    srand(1337);


    //
    // Wstawianie
    //

    // losowanie kluczy
    klucze = generujKlucze(ilosc_kluczy, czy_losowy);
    begin = clock();

    // wstawianie danych
    for (int i=0; i<ilosc_kluczy; i++) {
        drzewo.insert(klucze.at(i));
    }

    end = clock();
    time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    std::cout << "Wstawianie " << ilosc_kluczy<< " elementow drzewo: : " << time_spent << std::endl;

    //
    // Wyszukiwanie
    //

    // losowanie kluczy
    klucze.empty();
    klucze = generujKlucze(ilosc_kluczy, czy_losowy);
    begin = clock();
    // wstawianie danych
    for (int i=0; i<ilosc_kluczy; i++) {
        drzewo.find(klucze.at(i));
    }
    end = clock();
    time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    std::cout << "Wyszukiwanie " << ilosc_kluczy << " elementow drzewo: : " << time_spent << std::endl;

    //
    // Usuwanie
    //
    // losowanie kluczy
    klucze.empty();
    klucze = generujKlucze(ilosc_kluczy, true);
    begin = clock();
    // wstawianie danych
    for (int i=0; i<ilosc_kluczy; i++) {
        drzewo.remove(klucze.at(i));
    }
    end = clock();
    time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    std::cout << "Usuwanie " << ilosc_kluczy << " elementow drzewo: : " << time_spent << std::endl;

    return 0;
}
#include "BSTNode.h"
#include <string>


BSTNode::BSTNode(int key) : key(key)
{
	left = nullptr;
	right = nullptr;
}

BSTNode::~BSTNode()
{
}

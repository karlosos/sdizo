#define _CRT_SECURE_NO_DEPRECATE
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <ctime>
#include <set>
#include <time.h>

struct Struktura {
	int i;
	char c;
	float f;
};

Struktura** losowanie(int N);
void kasowanie(Struktura** tab, int N);
void sortowanie(Struktura** tab, int N);
int zliczanie(Struktura** tab, int N, char c);

int randomInt(int startowa_liczba = -1000, int ostatnia_liczba = 9000) {
	int ile_liczb_w_przedziale = ostatnia_liczba - startowa_liczba;
	int wylosowana_liczba = (std::rand() % ile_liczb_w_przedziale) + startowa_liczba;
	return wylosowana_liczba;
}

char randomChar(char startowy_znak = 'B', char ostatni_znak = 'X') {
	int ile_znakow_w_przedziale = ostatni_znak - startowy_znak;
	char wylosowany_znak = std::rand() % ile_znakow_w_przedziale + startowy_znak;
	return wylosowany_znak;
}

int main() {
	clock_t begin, end;
	double time_spent;
	begin = clock();

	srand(time(NULL));

	int N;
	char X;

	FILE* fp = fopen("inlab01.txt", "r");
	if (fp == NULL)
		return -1;
	fscanf(fp, "%d %c", &N, &X);
	fclose(fp);

	Struktura** tab = losowanie(N);
	sortowanie(tab, N);

	for (int i = 0; i < 20; i++) {
		std::cout << tab[i]->i << " " << tab[i]->c << " " << tab[i]->f << std::endl;
	}

	std::cout << "Zliczanie znaku: " << X << ": " << zliczanie(tab, N, X) << std::endl;
	kasowanie(tab, N);

	end = clock();
	time_spent = (double)(end - begin);

	std::cout << "Czas wykonania calego programu:" << time_spent << std::endl;
	std::system("pause");
	return 0;
}

Struktura** losowanie(int N) {
	struct Struktura** tab = (struct Struktura**) malloc(N * sizeof(struct Struktura*));
	std::set<int> wykluczone;
	for (int i = 0; i < N; i++) {
		tab[i] = (struct Struktura*) malloc(sizeof(struct Struktura));

		// dodawanie unikalnych i
		int struct_i;
		do {
			struct_i = randomInt();
		} while (wykluczone.find(struct_i) != wykluczone.end());

		wykluczone.insert(struct_i);
		tab[i]->i = struct_i;

		tab[i]->c = randomChar();
		tab[i]->f = 1000.f + i;
	}
	return tab;
}

void kasowanie(Struktura** tab, int N) {
	for (int i = 0; i < N; i++) {
		free(tab[i]);
	}
	free(tab);
}

void sortowanie(Struktura** tab, int N) {
	for (int i = 0; i < N; i++) {
		bool flag = 0;
		for (int j = 0; j < N - 1; j++) {
			if (tab[j]->i > tab[j + 1]->i) {
				Struktura* tmp_strukt = tab[j+1];
				tab[j + 1] = tab[j];
				tab[j] = tmp_strukt;
				flag = 1;
			}
		}
		if (flag == 0)
			return;
	}
}

int zliczanie(Struktura** tab, int N, char c) {
	int zliczenia = 0;
	for (int i = 0; i < N; i++) {
		if (tab[i]->c == c)
			zliczenia++;
	}
	return zliczenia;
}
#pragma once
#include <iostream>
#include "Node.h"

class BidirectionalCircularList
{
public:
	BidirectionalCircularList();
	~BidirectionalCircularList();
	bool append(int key);
	void appendRandom(int quantity, int start = 99, int stop = 99999);
	void remove(int key);
	void removeAll();
	void printHead(int count);
	void printTail(int count);
	Node* find(int key);
	int getLength() const;
	Node* getFirst() const;
	Node* getLast() const;

private:
	Node* first;
	Node* last;
	int length;
	bool isEmpty();
	bool add(int key);
	void addToEmptyList(int key);
	void addAsFirstElement(int key);
	void addAsLastElement(int key);
	void addBeforeCursor(int key, Node* cursor);
	void removeOnly();
	void removeFirst();
	void removeLast();
	void removeBeforeCursor(Node* cursor);
	bool isKeyPossibleInList(int key);
};


#pragma once
#include <iostream>
#include <random>
class Node
{
public:
	Node(int k);
	~Node();
	const int key;
	double d;
	char c;
	Node* next;
	Node* prev;
};
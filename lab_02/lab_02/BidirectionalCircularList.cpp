#include "BidirectionalCircularList.h"
#include "Utils.h"

BidirectionalCircularList::BidirectionalCircularList()
{
	first = nullptr;
	last = nullptr;
	length = 0;
}

BidirectionalCircularList::~BidirectionalCircularList()
{
	removeAll();
}

bool BidirectionalCircularList::append(int key)
{
	bool flag = add(key);
	if (!flag) {
		std::cout << "Nie mozna dodac klucza: " << std::endl;
	}
	return flag;
}

bool BidirectionalCircularList::add(int key) 
{
	// list is epmty
	if (length == 0) {
		addToEmptyList(key);
		return true;
	}
	// new is min
	if (key < first->key) {
		addAsFirstElement(key);
		return true;
	}
	// new is max
	else if (key > last->key) {
		addAsLastElement(key);
		return true;
	}

	Node* cursor = first;
	for (int i = 0; i < length; i++) {
		// key is not unique (already in list)
		if (key == cursor->key) {
			return false;
		}
		if (key < cursor->key) {
			addBeforeCursor(key, cursor);
			return true;
		}
		cursor = cursor->next;
	}
	return false;
}

void BidirectionalCircularList::addToEmptyList(int key)
{
	Node* node = new Node(key);
	first = node;
	last = node;
	length = 1;
}

void BidirectionalCircularList::addAsFirstElement(int key)
{
	Node* node = new Node(key);
	node->next = first;
	first->prev = node;
	last->next = node;
	node->prev = last;
	first = node;
	length++;
}

void BidirectionalCircularList::addAsLastElement(int key)
{
	Node* node = new Node(key);
	last->next = node;
	node->prev = last;
	node->next = first;
	first->prev = node;
	last = node;
	length++;
}

void BidirectionalCircularList::addBeforeCursor(int key, Node* cursor)
{
	Node* node = new Node(key);
	Node* before = cursor->prev;
	node->next = cursor;
	cursor->prev = node;

	before->next = node;
	node->prev = before;

	length++;
}

void BidirectionalCircularList::appendRandom(int quantity, int start, int stop)
{
	for (int i = 0; i < quantity; i++) {
		bool flag = false;
		while (!flag) {
			//flag = append(randomInt(start, stop));
			flag = add(randomInt(start, stop));
		}
	}
}

void BidirectionalCircularList::remove(int key)
{
	if (!isEmpty()) {
		if (first->key == key) {
			if (length == 1) {
				removeOnly();
				return;
			}
			removeFirst();
			return;
		}
		else if (last->key == key) {
			removeLast();
			return;
		}

		Node* cursor = first;
		for (int i = 0; i < length; i++) {
			if (cursor->key == key) {
				removeBeforeCursor(cursor);
				return;
			}
		}
		std::cout << "Nie znaleziono klucza: " << key << std::endl;
	}
	else {
		std::cout << "Lista jest pusta." << std::endl;
	}

}

void BidirectionalCircularList::removeOnly()
{
	delete(first);
	first = nullptr;
	last = nullptr;
	length = 0;
}

void BidirectionalCircularList::removeFirst()
{
	Node* node = first;
	last->next = first->next;
	first->next->prev = last;
	first = first->next;
	delete(node);
	length--;
}

void BidirectionalCircularList::removeLast()
{
	Node* node = last;
	last->prev->next = first;
	first->prev = last->prev;
	last = last->prev;
	delete(node);
	length--;
}

void BidirectionalCircularList::removeBeforeCursor(Node* cursor)
{
	cursor->prev->next = cursor->next;
	cursor->next->prev = cursor->prev;
	delete(cursor);
	length--;
}

void BidirectionalCircularList::removeAll()
{
	Node* cursor = first;
	for (int i = 0; i < length; i++) {
		Node* cursor_tmp = cursor;
		cursor = cursor->next;
		delete(cursor_tmp);
	}
	first = nullptr;
	last = nullptr;
	length = 0;
}

Node* BidirectionalCircularList::find(int key)
{
	if (!isEmpty()) {
		if (isKeyPossibleInList(key)) {
			Node* cursor = first;
			for (int i = 0; i < length; i++) {
				if (cursor->key == key) {
					return cursor;
				}
			}
		}
	}
	std::cout << "Nie znaleziono klucza: " << key << std::endl;
	return nullptr;
}

bool BidirectionalCircularList::isKeyPossibleInList(int key)
{
	return (key >= first->key && key <= last->key);
}

void BidirectionalCircularList::printHead(int count)
{
	std::cout << "Wyswietlanie pierwszych " << count << " elementow." << std::endl;
	Node* cursor = first;
	for (int i = 0; i < count && i < length; i++) {
		std::cout << cursor->key << " " << cursor->d << std::endl;
		cursor = cursor->next;
	}
}

void BidirectionalCircularList::printTail(int count)
{
	std::cout << "Wyswietlanie ostatnich " << count << " elementow." << std::endl;
	Node* cursor = last;
	for (int i = 0; i < count && i < length; i++) {
		std::cout << cursor->key << " " << cursor->d << std::endl;
		cursor = cursor->prev;
	}
}
bool BidirectionalCircularList::isEmpty()
{
	return length==0;
}

int BidirectionalCircularList::getLength() const
{
	return length;
}

Node* BidirectionalCircularList::getFirst() const
{
	return first;
}

Node * BidirectionalCircularList::getLast() const
{
	return first->prev;
}
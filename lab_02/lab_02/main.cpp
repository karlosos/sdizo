#include <ctime>
#include <random>
#include "Node.h"
#include "BidirectionalCircularList.h"

int main() {
	// keys from file
	int keys[6];
	FILE* fp = fopen("inlab02.txt", "r");
	if (fp == NULL)
		return -1;
	fscanf(fp, "%d %d %d %d %d %d", &keys[0], &keys[1], &keys[2], &keys[3], &keys[4], &keys[5]);
	fclose(fp);

	// clock start
	clock_t begin, end;
	double time_spent;
	begin = clock();

	// initialize list
	BidirectionalCircularList list;

	// find key k1
	list.find(keys[1]);

	// insert x elements to list
	list.appendRandom(keys[0]);

	// print size of list
	std::cout << "Size: " << list.getLength() << std::endl;

	// print first 20
	list.printHead(20);

	list.append(keys[2]);
	list.printHead(20);

	list.append(keys[3]);
	list.printHead(20);

	list.append(keys[4]);
	list.printHead(20);

	list.append(keys[5]);
	list.printHead(20);

	list.remove(keys[3]);
	list.printHead(20);

	list.remove(keys[2]);
	list.printHead(20);

	list.remove(keys[5]);
	std::cout << "Size: " << list.getLength() << std::endl;

	list.find(keys[5]);

	list.printTail(11);
	list.removeAll();
	list.printTail(11);

	std::cout << "Size: " << list.getLength() << std::endl;

	end = clock();
	time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
	std::cout << "Time spent: " << time_spent << std::endl;

	std::system("pause");
	return 0;
}
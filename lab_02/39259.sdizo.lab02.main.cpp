// SDIZO I1 212A LAB02
// Karol Dzialowski
// dk39259@zut.edu.pl

/////////////////////////
/////////////////////////
// MAIN.CPP
/////////////////////////
/////////////////////////

#include <ctime>
#include <random>
#include "Node.h"
#include "BidirectionalCircularList.h"

int main() {
	// keys from file
	int keys[6];
	FILE* fp = fopen("inlab02.txt", "r");
	if (fp == NULL)
		return -1;
	fscanf(fp, "%d %d %d %d %d %d", &keys[0], &keys[1], &keys[2], &keys[3], &keys[4], &keys[5]);
	fclose(fp);

	// clock start
	clock_t begin, end;
	double time_spent;
	begin = clock();

	// initialize list
	BidirectionalCircularList list;

	// find key k1
	list.find(keys[1]);

	// insert x elements to list
	list.appendRandom(keys[0]);

	// print size of list
	std::cout << "Size: " << list.getLength() << std::endl;

	// print first 20
	list.printHead(20);

	list.append(keys[2]);
	list.printHead(20);

	list.append(keys[3]);
	list.printHead(20);

	list.append(keys[4]);
	list.printHead(20);

	list.append(keys[5]);
	list.printHead(20);

	list.remove(keys[3]);
	list.printHead(20);

	list.remove(keys[2]);
	list.printHead(20);

	list.remove(keys[5]);
	std::cout << "Size: " << list.getLength() << std::endl;

	list.find(keys[5]);

	list.printTail(11);
	list.removeAll();
	list.printTail(11);

	std::cout << "Size: " << list.getLength() << std::endl;

	end = clock();
	time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
	std::cout << "Time spent: " << time_spent << std::endl;

	std::system("pause");
	return 0;
}

/////////////////////////
/////////////////////////
// Node.h
/////////////////////////
/////////////////////////

#pragma once
#include <iostream>
#include <random>
class Node
{
public:
	Node(int k);
	~Node();
	const int key;
	double d;
	char c;
	Node* next;
	Node* prev;
};

/////////////////////////
/////////////////////////
// Node.cpp
/////////////////////////
/////////////////////////

#include "Node.h"
#include "Utils.h"

Node::Node(int k) : key(k)
{
	d = randomInt()*0.125;
	c = 'T';
	//std::cout << "Dodane klucz: " << k << std::endl;
	next = this;
	prev = this;
}


Node::~Node()
{
	//std::cout << "Usunieto klucz: " << key << std::endl;
}

/////////////////////////
/////////////////////////
// BidirectionalCircularList.h
/////////////////////////
/////////////////////////

#pragma once
#include <iostream>
#include "Node.h"

class BidirectionalCircularList
{
public:
	BidirectionalCircularList();
	~BidirectionalCircularList();
	bool append(int key);
	void appendRandom(int quantity, int start = 99, int stop = 99999);
	void remove(int key);
	void removeAll();
	void printHead(int count);
	void printTail(int count);
	Node* find(int key);
	int getLength() const;
	Node* getFirst() const;
	Node* getLast() const;

private:
	Node* first;
	Node* last;
	int length;
	bool isEmpty();
	bool add(int key);
	void addToEmptyList(int key);
	void addAsFirstElement(int key);
	void addAsLastElement(int key);
	void addBeforeCursor(int key, Node* cursor);
	void removeOnly();
	void removeFirst();
	void removeLast();
	void removeBeforeCursor(Node* cursor);
	bool isKeyPossibleInList(int key);
};

/////////////////////////
/////////////////////////
// BidirectionalCircularList.cpp
/////////////////////////
/////////////////////////

#include "BidirectionalCircularList.h"
#include "Utils.h"

BidirectionalCircularList::BidirectionalCircularList()
{
	first = nullptr;
	last = nullptr;
	length = 0;
}

BidirectionalCircularList::~BidirectionalCircularList()
{
	removeAll();
}

bool BidirectionalCircularList::append(int key)
{
	bool flag = add(key);
	if (!flag) {
		std::cout << "Nie mozna dodac klucza: " << std::endl;
	}
	return flag;
}

bool BidirectionalCircularList::add(int key) 
{
	// list is epmty
	if (length == 0) {
		addToEmptyList(key);
		return true;
	}
	// new is min
	if (key < first->key) {
		addAsFirstElement(key);
		return true;
	}
	// new is max
	else if (key > last->key) {
		addAsLastElement(key);
		return true;
	}

	Node* cursor = first;
	for (int i = 0; i < length; i++) {
		// key is not unique (already in list)
		if (key == cursor->key) {
			return false;
		}
		if (key < cursor->key) {
			addBeforeCursor(key, cursor);
			return true;
		}
		cursor = cursor->next;
	}
	return false;
}

void BidirectionalCircularList::addToEmptyList(int key)
{
	Node* node = new Node(key);
	first = node;
	last = node;
	length = 1;
}

void BidirectionalCircularList::addAsFirstElement(int key)
{
	Node* node = new Node(key);
	node->next = first;
	first->prev = node;
	last->next = node;
	node->prev = last;
	first = node;
	length++;
}

void BidirectionalCircularList::addAsLastElement(int key)
{
	Node* node = new Node(key);
	last->next = node;
	node->prev = last;
	node->next = first;
	first->prev = node;
	last = node;
	length++;
}

void BidirectionalCircularList::addBeforeCursor(int key, Node* cursor)
{
	Node* node = new Node(key);
	Node* before = cursor->prev;
	node->next = cursor;
	cursor->prev = node;

	before->next = node;
	node->prev = before;

	length++;
}

void BidirectionalCircularList::appendRandom(int quantity, int start, int stop)
{
	for (int i = 0; i < quantity; i++) {
		bool flag = false;
		while (!flag) {
			//flag = append(randomInt(start, stop));
			flag = add(randomInt(start, stop));
		}
	}
}

void BidirectionalCircularList::remove(int key)
{
	if (!isEmpty()) {
		if (first->key == key) {
			if (length == 1) {
				removeOnly();
				return;
			}
			removeFirst();
			return;
		}
		else if (last->key == key) {
			removeLast();
			return;
		}

		Node* cursor = first;
		for (int i = 0; i < length; i++) {
			if (cursor->key == key) {
				removeBeforeCursor(cursor);
				return;
			}
		}
		std::cout << "Nie znaleziono klucza: " << key << std::endl;
	}
	else {
		std::cout << "Lista jest pusta." << std::endl;
	}

}

void BidirectionalCircularList::removeOnly()
{
	delete(first);
	first = nullptr;
	last = nullptr;
	length = 0;
}

void BidirectionalCircularList::removeFirst()
{
	Node* node = first;
	last->next = first->next;
	first->next->prev = last;
	first = first->next;
	delete(node);
	length--;
}

void BidirectionalCircularList::removeLast()
{
	Node* node = last;
	last->prev->next = first;
	first->prev = last->prev;
	last = last->prev;
	delete(node);
	length--;
}

void BidirectionalCircularList::removeBeforeCursor(Node* cursor)
{
	cursor->prev->next = cursor->next;
	cursor->next->prev = cursor->prev;
	delete(cursor);
	length--;
}

void BidirectionalCircularList::removeAll()
{
	Node* cursor = first;
	for (int i = 0; i < length; i++) {
		Node* cursor_tmp = cursor;
		cursor = cursor->next;
		delete(cursor_tmp);
	}
	first = nullptr;
	last = nullptr;
	length = 0;
}

Node* BidirectionalCircularList::find(int key)
{
	if (!isEmpty()) {
		if (isKeyPossibleInList(key)) {
			Node* cursor = first;
			for (int i = 0; i < length; i++) {
				if (cursor->key == key) {
					return cursor;
				}
			}
		}
	}
	std::cout << "Nie znaleziono klucza: " << key << std::endl;
	return nullptr;
}

bool BidirectionalCircularList::isKeyPossibleInList(int key)
{
	return (key >= first->key && key <= last->key);
}

void BidirectionalCircularList::printHead(int count)
{
	std::cout << "Wyswietlanie pierwszych " << count << " elementow." << std::endl;
	Node* cursor = first;
	for (int i = 0; i < count && i < length; i++) {
		std::cout << cursor->key << " " << cursor->d << std::endl;
		cursor = cursor->next;
	}
}

void BidirectionalCircularList::printTail(int count)
{
	std::cout << "Wyswietlanie ostatnich " << count << " elementow." << std::endl;
	Node* cursor = last;
	for (int i = 0; i < count && i < length; i++) {
		std::cout << cursor->key << " " << cursor->d << std::endl;
		cursor = cursor->prev;
	}
}
bool BidirectionalCircularList::isEmpty()
{
	return length==0;
}

int BidirectionalCircularList::getLength() const
{
	return length;
}

Node* BidirectionalCircularList::getFirst() const
{
	return first;
}

Node * BidirectionalCircularList::getLast() const
{
	return first->prev;
}

/////////////////////////
/////////////////////////
// Utils.h
/////////////////////////
/////////////////////////

#pragma once
#ifndef UTILS_H
#define UTILS_H

int randomInt(int startowa_liczba = 0, int ostatnia_liczba = 100);

#endif

/////////////////////////
/////////////////////////
// Utils.cpp
/////////////////////////
/////////////////////////

#pragma once
#include <random>
#include "Utils.h"

int randomInt(int startowa_liczba, int ostatnia_liczba) {
	return rand() % (ostatnia_liczba - startowa_liczba) + startowa_liczba;
}

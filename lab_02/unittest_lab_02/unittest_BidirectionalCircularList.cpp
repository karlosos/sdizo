// unittest_SimpleMath.cpp : Defines the entry point for the console application.

#include "gtest/gtest.h"
#include "../lab_02/BidirectionalCircularList.h"
#include "../lab_02/Utils.h"

bool test_continuity(BidirectionalCircularList &list) {
	Node* cursor = list.getFirst();
	int before_key = INT_MIN;
	for (int i = 0; i < list.getLength(); i++) {
		if (before_key > cursor->key)
			return false;
		cursor = cursor->next;
	}
	
	cursor = list.getLast();
	before_key = INT_MAX;
	for (int i = list.getLength(); i > 0; i--) {
		if (before_key < cursor->key)
			return false;
		cursor = cursor->prev;
	}

	return true;
}

TEST(bidirectionCircularList_test, list_len_is_0_after_create)
{
	srand(time(NULL));
	BidirectionalCircularList list;
	EXPECT_EQ(0, list.getLength());
	EXPECT_EQ(list.getFirst(), nullptr);
	ASSERT_TRUE(test_continuity(list));
}

TEST(bidirectionCircularList_test, list_len_is_1_after_append)
{
	srand(time(NULL));
	BidirectionalCircularList list;
	list.append(1);
	EXPECT_EQ(1, list.getLength());
	ASSERT_TRUE(test_continuity(list));
}

TEST(bidirectionCircularList_test, list_len_is_greater_after_append)
{
	srand(time(NULL));
	BidirectionalCircularList list;
	list.append(1);
	int len_before = list.getLength();
	ASSERT_EQ(1, list.getLength());
	list.append(2);
	EXPECT_EQ(len_before+1, list.getLength());
	ASSERT_TRUE(test_continuity(list));
}

TEST(append_random_elements, append_1_random_element)
{
	srand(time(NULL));
	BidirectionalCircularList list;
	int len_before = list.getLength();
	ASSERT_EQ(len_before, list.getLength());
	list.appendRandom(1);
	ASSERT_EQ(len_before + 1, list.getLength());
	ASSERT_TRUE(test_continuity(list));
}

TEST(append_random_elements, append_100_random_elements)
{
	
	srand(time(NULL));
	BidirectionalCircularList list;
	int len_before = list.getLength();
	ASSERT_EQ(len_before, list.getLength());
	list.appendRandom(100);
	ASSERT_EQ(len_before + 100, list.getLength());
	ASSERT_TRUE(test_continuity(list));
}

TEST(append_random_elements, append_random_quantity_of_elements)
{
	for (int i = 0; i < 10; i++) {
		srand(time(NULL));
		int quantity = randomInt(0, 100);
		BidirectionalCircularList list;
		int len_before = list.getLength();
		ASSERT_EQ(len_before, list.getLength());
		list.appendRandom(quantity);
		ASSERT_EQ(len_before + quantity, list.getLength());
		ASSERT_TRUE(test_continuity(list));
	}
}

TEST(find_element, not_find_in_empty)
{
	BidirectionalCircularList list;
	ASSERT_EQ(list.find(1),nullptr);
}

TEST(find_element, find_in_one_length)
{
	BidirectionalCircularList list;
	list.append(1);
	ASSERT_EQ(list.find(1)->key,1);
}

TEST(find_element, not_find_in_one_length)
{
	BidirectionalCircularList list;
	list.append(2);
	ASSERT_EQ(list.find(1), nullptr);
}

TEST(find_element, not_find_if_greater_than_last)
{
	BidirectionalCircularList list;
	list.appendRandom(100);
	ASSERT_EQ(list.find(999999), nullptr);
}

TEST(find_element, not_find_if_lesser_than_min)
{
	BidirectionalCircularList list;
	list.appendRandom(100);
	ASSERT_EQ(list.find(1), nullptr);
}

TEST(remove_all, remove_empty)
{
	BidirectionalCircularList list;
	list.removeAll();
	ASSERT_EQ(list.getLength(), 0);
}

TEST(remove, remove_in_empty)
{
	BidirectionalCircularList list;
	list.remove(3);
	ASSERT_EQ(list.getLength(), 0);
}

TEST(remove, remove_only_element)
{
	BidirectionalCircularList list;
	list.append(3);
	ASSERT_EQ(list.getLength(), 1);
	Node* first = list.getLast();
	Node* last = list.getLast();
	ASSERT_EQ(first, last);
	list.remove(3);
	ASSERT_EQ(list.getFirst(), nullptr);
	ASSERT_EQ(list.getLast(), nullptr);
	ASSERT_EQ(list.getLength(), 0);
}
#include "gtest/gtest.h"
#include "../Lab_04/AVL.h"
#include "../Lab_04/AVLNode.h"
#include "../Lab_04/Utils.h"
#include <ctime>

int height(AVLNode* node)
{
       if (node == nullptr)
               return -1;
       else
               return std::max(height(node->left), height(node->right)) + 1;
}

//
// AVLNode tests
//
TEST(Node, has_key_value)
{
	AVLNode node = AVLNode(30);
	EXPECT_EQ(node.key, 30);
}

TEST(Node, has_balance_factor)
{
	AVLNode* node = new AVLNode(30);
	EXPECT_EQ(getBalanceFactor(node), 0);
}

TEST(Node, has_pointer_to_left_child)
{
	AVLNode node = AVLNode(30);
	EXPECT_EQ(node.left, nullptr);
}

TEST(Node, has_pointer_to_right_child)
{
	AVLNode node = AVLNode(30);
	EXPECT_EQ(node.right, nullptr);
}

TEST(Node, parse_key_to_char_table)
{
	AVLNode node = AVLNode(4302);
	//EXPECT_STREQ("4302", node.getTab());
}

//
// AVLTree tests
//
TEST(AVL_Init, root_is_nullptr)
{
	AVL tree = AVL();
	EXPECT_EQ(tree.root, nullptr);
	EXPECT_EQ(tree.inOrder(), 0);
}

TEST(AVL_Insert, insert_to_empty_tree)
{
	AVL* tree = new AVL();
	tree->insertAsLeaf(87);
	EXPECT_EQ(tree->root->key, 87);
	//EXPECT_STREQ("87", tree->root->getTab());
	delete tree;
}

TEST(AVL_Insert, insert_to_the_right)
{
	AVL* tree = new AVL();
	tree->insertAsLeaf(42);
	tree->insertAsLeaf(64);
	EXPECT_EQ(tree->root->right->key, 64);
	delete tree;
}

TEST(AVL_Insert, insert_to_the_left)
{
	AVL* tree = new AVL();
	tree->insertAsLeaf(42);
	tree->insertAsLeaf(21);
	EXPECT_EQ(tree->root->left->key, 21);
	delete tree;
}

TEST(AVL_Insert, test_stack)
{
	AVL tree = AVL();
	tree.insertAsLeaf(54);
	tree.insertAsLeaf(1);
	tree.insertAsLeaf(64);
	tree.insertAsLeaf(534);
	tree.insertAsLeaf(18);
	tree.insertAsLeaf(53);
	tree.insertAsLeaf(43);
	std::vector <AVLNode*> road_to_root = tree.insertAsLeaf(44);
	EXPECT_EQ(road_to_root.size(), 6);
}

TEST(AVL_Rotate, rotate_right) {
	AVL tree = AVL();
	tree.insertAsLeaf(1);
	tree.insertAsLeaf(5);
	tree.insertAsLeaf(6);
	tree.insertAsLeaf(3);
	tree.insertAsLeaf(2);
	tree.insertAsLeaf(4);

	AVLNode* grandfather = tree.find(1);
	AVLNode* parent = tree.find(5);
	AVLNode* child = tree.find(3);
	AVLNode* child_old_right = child->right;

	tree.rotateRight(grandfather, parent, child);
	tree.preOrder();
	EXPECT_EQ(grandfather->right, child);
	EXPECT_EQ(child->right, parent);
	EXPECT_EQ(parent->left, child_old_right);

	tree.~AVL();

	tree.insertAsLeaf(5);
	tree.insertAsLeaf(3);
	tree.insertAsLeaf(1);
	tree.insertAsLeaf(2);
	tree.insertAsLeaf(4);
	tree.insertAsLeaf(8);

	grandfather = tree.find(5);
	parent = tree.find(3);
	child = tree.find(1);
	child_old_right = child->right;

	tree.rotateRight(grandfather, parent, child);
	tree.preOrder();
	EXPECT_EQ(grandfather->left, child);
	EXPECT_EQ(child->right, parent);
	EXPECT_EQ(parent->left, child_old_right);

	tree.~AVL();

	tree.insertAsLeaf(8);
	tree.insertAsLeaf(6);
	tree.insertAsLeaf(7);
	tree.insertAsLeaf(9);

	grandfather = nullptr;
	parent = tree.find(8);
	child = tree.find(6);
	child_old_right = child->right;
	tree.rotateRight(nullptr, parent, child);
	tree.preOrder();
	EXPECT_EQ(tree.root, child);
	EXPECT_EQ(child->right, parent);
	EXPECT_EQ(parent->left, child_old_right);

	tree.~AVL();

	tree.insertAsLeaf(10);
	tree.insertAsLeaf(8);
	tree.insertAsLeaf(9);
	tree.insertAsLeaf(7);

	grandfather = nullptr;
	parent = tree.find(10);
	child = tree.find(8);
	child_old_right = child->right;
	tree.rotateRight(nullptr, parent, child);
	tree.preOrder();
	EXPECT_EQ(tree.root, child);
	EXPECT_EQ(child->right, parent);
	EXPECT_EQ(parent->left, child_old_right);

	child = tree.find(8);
	child_old_right = child->right;
	tree.rotateRight(nullptr, nullptr, child);
	EXPECT_EQ(tree.root, child);
	EXPECT_EQ(child->right, child_old_right);
	tree.preOrder();
}

TEST(AVL_Rotate, rotate_left)
{
	AVL tree = AVL();
	tree.insertAsLeaf(9);
	tree.insertAsLeaf(15);
	tree.insertAsLeaf(14);
	tree.insertAsLeaf(17);
	tree.insertAsLeaf(16);
	tree.insertAsLeaf(20);

	AVLNode* grandfather = tree.find(9);
	AVLNode* parent = tree.find(15);
	AVLNode* child = tree.find(17);
	AVLNode* child_old_left = child->left;

	tree.rotateLeft(grandfather, parent, child);
	tree.preOrder();

	EXPECT_EQ(grandfather->right, child);
	EXPECT_EQ(child->left, parent);
	EXPECT_EQ(parent->right, child_old_left);

	tree.~AVL();
	tree.insertAsLeaf(15);
	tree.insertAsLeaf(9);
	tree.insertAsLeaf(3);
	tree.insertAsLeaf(12);
	tree.insertAsLeaf(11);
	tree.insertAsLeaf(14);

	grandfather = tree.find(15);
	parent = tree.find(9);
	child = tree.find(12);
	child_old_left = child->left;
	tree.rotateLeft(grandfather, parent, child);
	tree.preOrder();
	EXPECT_EQ(grandfather->left, child);
	EXPECT_EQ(child->left, parent);
	EXPECT_EQ(parent->right, child_old_left);

	tree.~AVL();
	tree.insertAsLeaf(8);
	tree.insertAsLeaf(7);
	tree.insertAsLeaf(19);
	tree.insertAsLeaf(10);

	grandfather = nullptr;
	parent = tree.find(8);
	child = tree.find(19);
	child_old_left = child->left;
	tree.rotateLeft(grandfather, parent, child);
	tree.preOrder();
	EXPECT_EQ(tree.root, child);
	EXPECT_EQ(child->left, parent);
	EXPECT_EQ(parent->right, child_old_left);

	tree.~AVL();
	tree.insertAsLeaf(5);
	tree.insertAsLeaf(8);
	tree.insertAsLeaf(7);
	tree.insertAsLeaf(9);

	grandfather = nullptr;
	parent = tree.find(5);
	child = tree.find(8);
	child_old_left = child->left;
	tree.rotateLeft(grandfather, parent, child);
	tree.preOrder();
	EXPECT_EQ(tree.root, child);
	EXPECT_EQ(child->left, parent);
	EXPECT_EQ(parent->right, child_old_left);
}

TEST(AVL_Insert, balance)
{
	AVL tree = AVL();
	tree.insert(13);
    tree.preOrder();
	tree.insert(10);
    tree.preOrder();
	tree.insert(5);
    tree.preOrder();
	tree.insert(4);
    tree.preOrder();
	tree.insert(6);
    tree.preOrder();
	tree.insert(11);
    tree.preOrder();
	tree.insert(15);
    tree.preOrder();
	tree.insert(16);
    tree.preOrder();
    EXPECT_GT(height(tree.root->right) - height(tree.root->left), -2);
	EXPECT_LT(height(tree.root->right) - height(tree.root->left), 2);
}

TEST(AVL_Insert, randomTest)
{
	int keys[] = {17, 28, 93, 32, 42, 94, 23, 12, 4, 92, 8, 10, 9, 2, 5, 6, 19, 48, 342, 23, 234, 432, 523, 423,632,354,2435,
	6342,23,12,3234,1234,152,63542,1235,3246,324,51234,236,4743,5123,3462,315,2346,1325,362,231,53264,346123,5236,2135,2365};
	int size = sizeof(keys)/sizeof(int);
	//EXPECT_EQ(size, 18);
	AVL tree = AVL();
	for (int i=0; i<size; i++) {
		tree.insert(keys[i]);
		tree.preOrder();
	}
	EXPECT_GT(height(tree.root->right) - height(tree.root->left), -2);
	EXPECT_LT(height(tree.root->right) - height(tree.root->left), 2);
}

TEST(AVL_Height, heigh_in_empty)
{
	AVL tree = AVL();

	int tree_h = height(tree.root);
	EXPECT_EQ(tree_h, -1);
}

TEST(AVL_Insert, insert_20)
{
	AVL tree = AVL();
	tree.insertMany(20);
	EXPECT_EQ(tree.preOrder(), 20);
}
TEST(AVL_Insert, insert_many)
{
	AVL tree = AVL();
	tree.insertMany(400);

	EXPECT_GT(height(tree.root->right) - height(tree.root->left), -2);
	EXPECT_LT(height(tree.root->right) - height(tree.root->left), 2);

	tree.insertMany(600);
	EXPECT_GT(height(tree.root->right) - height(tree.root->left), -2);
	EXPECT_LT(height(tree.root->right) - height(tree.root->left), 2);

	EXPECT_EQ(tree.preOrder(), 1000);
}

TEST(AVL_Insert, sample_tree)
{
	int keys[] = {64, 23, 29, 65, 66, 63, 22, 24, 25, 58};
	int size = sizeof(keys)/sizeof(int);
	//EXPECT_EQ(size, 18);
	AVL tree = AVL();
	for (int i=0; i<size; i++) {
		tree.insert(keys[i]);
	}

	EXPECT_EQ(tree.preOrder(), size);
}

TEST(AVL_Balance, temptest)
{
	//int keys[] = {17, 28, 93, 32, 42, 94, 23, 12, 4, 92, 8, 10, 9, 2, 5};
//	int keys[] = {12, 23, 1, 3, 4, 2, -8};
	int keys[] = {64, 23, 29, 65, 66, 63, 22, 24, 25, 58};
	int size = sizeof(keys)/sizeof(int);
	//EXPECT_EQ(size, 18);
	AVL tree = AVL();
	for (int i=0; i<size; i++) {
		tree.insert(keys[i]);
		tree.preOrder();
	}
	tree.preOrder();
}

TEST(AVL_Balance, jednorodna_z_prawej)
{
	AVL tree = AVL();
	tree.insert(64);
	tree.insert(23);
	tree.insert(29);
	tree.insert(65);
	tree.insert(66);
	EXPECT_EQ(tree.find(29)->right, tree.find(65));
	EXPECT_EQ(tree.find(65)->left, tree.find(64));
	EXPECT_EQ(tree.find(65)->right, tree.find(66));
}

TEST(AVL_Balance, niejednorodna_z_lewej)
{
	AVL tree = AVL();
	tree.insert(64);
	tree.insert(23);
	tree.insert(29);
	tree.insert(65);
	tree.insert(66);
	tree.insert(63);
	EXPECT_EQ(tree.root, tree.find(64));
	EXPECT_EQ(tree.root->left, tree.find(29));
	EXPECT_EQ(tree.root->right, tree.find(65));
	EXPECT_EQ(tree.find(29)->left, tree.find(23));
	EXPECT_EQ(tree.find(29)->right, tree.find(63));
	EXPECT_EQ(tree.find(65)->right, tree.find(66));
}


TEST(AVL_Remove, remove_not_existing)
{
	int keys[] = {64, 23, 29, 65, 66, 63, 22, 24, 25, 58};
	int size = sizeof(keys)/sizeof(int);
	//EXPECT_EQ(size, 18);
	AVL tree = AVL();
	for (int i=0; i<size; i++) {
		tree.insert(keys[i]);
	}

	EXPECT_EQ(tree.remove(666), false);
	EXPECT_EQ(tree.preOrder(), 10);
}

TEST(AVL_Remove, remove_only_one)
{
	AVL tree = AVL();
	tree.insert(20);
	EXPECT_EQ(tree.remove(20), true);
	EXPECT_EQ(tree.root, nullptr);
}

TEST(AVL_Remove, remove_in_empty_tree)
{
	AVL tree = AVL();
	EXPECT_EQ(tree.remove(20), false);
}

TEST(AVL_Remove, remove_with_left_child)
{
	int keys[] = {64, 23, 29, 65, 66, 63, 22, 24, 25, 58};
	int size = sizeof(keys)/sizeof(int);
	//EXPECT_EQ(size, 18);
	AVL tree = AVL();
	for (int i=0; i<size; i++) {
		tree.insert(keys[i]);
	}

	tree.remove(63);
	EXPECT_EQ(tree.preOrder(), size-1);
	EXPECT_GT(height(tree.root->right) - height(tree.root->left), -2);
	EXPECT_LT(height(tree.root->right) - height(tree.root->left), 2);
}

TEST(AVL_Remove, remove_with_right_child)
{
	int keys[] = {64, 23, 29, 65, 66, 63, 22, 24, 25, 58};
	int size = sizeof(keys)/sizeof(int);
	//EXPECT_EQ(size, 18);
	AVL tree = AVL();
	for (int i=0; i<size; i++) {
		tree.insert(keys[i]);
	}

	tree.remove(65);
	EXPECT_EQ(tree.preOrder(), size-1);
	EXPECT_GT(height(tree.root->right) - height(tree.root->left), -2);
	EXPECT_LT(height(tree.root->right) - height(tree.root->left), 2);
}

TEST(AVL_Remove, remove_leaf)
{

	int keys[] = {64, 23, 29, 65, 66, 63, 22, 24, 25, 58};
	int size = sizeof(keys)/sizeof(int);
	//EXPECT_EQ(size, 18);
	AVL tree = AVL();
	for (int i=0; i<size; i++) {
		tree.insert(keys[i]);
	}

	tree.remove(66);
	EXPECT_EQ(tree.preOrder(), size-1);
	EXPECT_GT(height(tree.root->right) - height(tree.root->left), -2);
	EXPECT_LT(height(tree.root->right) - height(tree.root->left), 2);
}

TEST(AVL_Remove, remove_with_precessor_as_left)
{

	int keys[] = {64, 23, 29, 65, 66, 63, 22, 24, 25, 58};
	int size = sizeof(keys)/sizeof(int);
	//EXPECT_EQ(size, 18);
	AVL tree = AVL();
	for (int i=0; i<size; i++) {
		tree.insert(keys[i]);
	}

	tree.remove(64);
	EXPECT_EQ(tree.preOrder(), size-1);
	EXPECT_GT(height(tree.root->right) - height(tree.root->left), -2);
	EXPECT_LT(height(tree.root->right) - height(tree.root->left), 2);
}

TEST(AVL_Remove, remove_with_precessor_as_grandchild)
{

	int keys[] = {64, 23, 29, 65, 66, 63, 22, 24, 25, 58, 26, 38, 87};
	int size = sizeof(keys)/sizeof(int);
	//EXPECT_EQ(size, 18);
	AVL tree = AVL();
	for (int i=0; i<size; i++) {
		tree.insert(keys[i]);
	}

	tree.remove(64);

	EXPECT_EQ(tree.preOrder(), size-1);
	EXPECT_GT(height(tree.root->right) - height(tree.root->left), -2);
	EXPECT_LT(height(tree.root->right) - height(tree.root->left), 2);
}

TEST(AVL_Remove, remove_many)
{
    srand(time(NULL));
    int tree_size = 15;
    std::vector <int> list;
    AVL tree;
    bool flag = false;
    int i = 0;
    while (i < tree_size) {
        int key = randomInt(0, 100);
        flag = tree.insert(key);
        if (flag) {
            //std::cout << "Dodano: " << key << std::endl;
            list.push_back(key);
            i++;
        }
    }

    std::random_shuffle(list.begin(), list.end());
    for (int i = 0; i < list.size(); i++) {
        tree.insert(list.at(i));
    }
    EXPECT_EQ(tree.preOrder(), tree_size);

    std::random_shuffle(list.begin(), list.end());
    for (int i=0; i<tree_size; i++) {
		tree.remove(list.at(i));
		tree.preOrder();
	}
	EXPECT_EQ(0, tree.preOrder());
}
std::vector<int> generujKlucze(int n, bool czy_losowy) {
	std::vector<int> klucze;
	// losowanie kluczy
	if (czy_losowy == 0) {
		// nie rozklad losowy
		int k = 1;
		for (int i=0; i<n; i++) {
			if (i%2) {
				klucze.push_back(k);
				k++;
			} else {
				klucze.push_back(randomInt(0, 1000000));
			}
		}
	} else {
		for (int i=0; i<n; i++) {
			klucze.push_back(randomInt(0, 1000000));
		}
	}
	return klucze;
}

TEST(TimeTest, insert)
{
	AVL drzewo;

	int ilosc_kluczy = 30000;
	bool czy_losowy = 0;
	std::vector<int> klucze;

	// losowanie kluczy
	klucze = generujKlucze(ilosc_kluczy, czy_losowy);
	clock_t begin, end;
	double time_spent;
	srand(1337);

	begin = clock();
	// wstawianie danych
	for (int i=0; i<ilosc_kluczy; i++) {
		drzewo.insert(klucze.at(i));
	}

	end = clock();
	time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
	std::cout << "Wstawianie " << ilosc_kluczy<< " elementow drzewo: : " << time_spent << std::endl;

	//
	// Wyszukiwanie
	//

	// losowanie kluczy
	klucze.empty();
	klucze = generujKlucze(ilosc_kluczy, true);
	begin = clock();
	for (int i=0; i<ilosc_kluczy; i++) {
		drzewo.find(klucze.at(i));
	}
	end = clock();
	time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
	std::cout << "Wyszukiwanie " << ilosc_kluczy << " elementow drzewo: : " << time_spent << std::endl;

	//
	// Usuwanie
	//
	// losowanie kluczy
	klucze.empty();
	klucze = generujKlucze(ilosc_kluczy, true);
	begin = clock();
	for (int i=0; i<ilosc_kluczy; i++) {
		drzewo.remove(klucze.at(i));
	}
	end = clock();
	time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
	std::cout << "Usuwanie " << ilosc_kluczy << " elementow drzewo: : " << time_spent << std::endl;
}
project(Lab_04Tests)
add_subdirectory(gtest/googletest)

include_directories(${gtest_SOURCE_DIR}/include ${gtest_SOURCE_DIR})
add_executable(runTests
        Lab_04Tests.cpp)
target_link_libraries(runTests gtest gtest_main)
target_link_libraries(runTests Lab_04)

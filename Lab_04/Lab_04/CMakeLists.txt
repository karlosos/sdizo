project(Lab_04)

set(HEADER_FILES
        Utils.h
        AVLNode.h
        AVL.h)

set(SOURCE_FILES
        Utils.cpp
        AVLNode.cpp
        AVL.cpp)

add_library(Lab_04 STATIC ${SOURCE_FILES} ${HEADER_FILES})
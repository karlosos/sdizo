#pragma once
//#include "gtest/gtest_prod.h"
#include "../Lab_04Tests/gtest/googletest/include/gtest/gtest_prod.h"
#include "AVLNode.h"
#include <stack>
#include <vector>
#include <queue>
#include <algorithm>
#include <iostream>
#include <ctime>
class AVL
{
public:
	AVL();
	~AVL();

	AVLNode* root;

	bool insert(int key);
	void insertMany(int quantity);
	bool remove(int key);
	AVLNode* find(int key);
	int inOrder();
	int postOrder();
	int preOrder();
private:
	int count;

	int inOrder(AVLNode *node);
	int preOrder(AVLNode *node);
	int postOrder(AVLNode *node);
	std::vector<AVLNode*> insertAsLeaf(int key);
	void clearTree(AVLNode* node);
	void rotateRight(AVLNode* grandfather, AVLNode* parent, AVLNode* child);
	void rotateLeft(AVLNode* grandfather, AVLNode* parent, AVLNode* child);
	void balanceAfterRemove(std::vector <AVLNode*> road_to_root);

	FRIEND_TEST(AVL_Insert, insert_to_empty_tree);
	FRIEND_TEST(AVL_Insert, insert_to_the_right);
	FRIEND_TEST(AVL_Insert, insert_to_the_left);
	FRIEND_TEST(AVL_Insert, test_stack);
	FRIEND_TEST(AVL_Rotate, rotate_right);
	FRIEND_TEST(AVL_Rotate, rotate_left);

};
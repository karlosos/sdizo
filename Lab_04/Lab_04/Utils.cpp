#pragma once
#include <random>
#include "Utils.h"

int randomInt(int startowa_liczba, int ostatnia_liczba) {
	return rand() % (ostatnia_liczba - startowa_liczba) + startowa_liczba;
}

int max(int left, int right) {
	return (left<right)?right:left;
}


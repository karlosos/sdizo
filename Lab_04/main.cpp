//
// Created by Karol Dzialowski on 25.11.2017.
//

#include "Lab_04/AVL.h"
#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <ctime>

int main() {
    int keys[5];
    FILE* fp = fopen("inlab_004.txt", "r");
    if (fp == NULL)
        return -1;
    fscanf(fp, "%d %d %d %d %d", &keys[0], &keys[1], &keys[2], &keys[3], &keys[4]);
    fclose(fp);

    // clock start
    clock_t begin, end;
    double time_spent;
    begin = clock();

    AVL tree = AVL();
    tree.remove(keys[1]);
    tree.insert(keys[1]);
    tree.insertMany(keys[0]);
    tree.inOrder();
    tree.preOrder();
    tree.insert(keys[2]);
    tree.inOrder();
    tree.insert(keys[3]);
    tree.insert(keys[4]);
    tree.remove(keys[1]);
    tree.preOrder();
    tree.find(keys[1]);
    tree.remove(keys[2]);
    tree.inOrder();
    tree.remove(keys[3]);
    tree.remove(keys[4]);

    end = clock();
    time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    std::cout << "Time spent: " << time_spent << std::endl;

    //std::system("pause");
    return 0;
}